<div class="header-section">
    <img class="img-logo" src="<?php echo template_uri();?>images/logo.png" alt="Logo Apresiasi Inovasi">
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="4"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active" style="background-image: url('<?php echo template_uri();?>images/slider/1.jpg')"></div>
            <div class="carousel-item" style="background-image: url('<?php echo template_uri();?>images/slider/2.jpg')"></div>
            <div class="carousel-item" style="background-image: url('<?php echo template_uri();?>images/slider/3.jpg')"></div>
            <div class="carousel-item" style="background-image: url('<?php echo template_uri();?>images/slider/4.jpg')"></div>
            <div class="carousel-item" style="background-image: url('<?php echo template_uri();?>images/slider/5.jpg')"></div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>